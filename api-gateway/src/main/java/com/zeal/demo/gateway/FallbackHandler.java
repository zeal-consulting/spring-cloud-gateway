package com.zeal.demo.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class FallbackHandler {

    @GetMapping("/listings/fallback")
    public Mono<String> listingsFallback() {
        return Mono.just("Listing service is currently unavailable. Please try again later.");
    }

    @RequestMapping(value = "/analytics/fallback", method = {RequestMethod.GET, RequestMethod.POST})
    public Mono<String> analyticsFallback() {
        return Mono.just("Analytics service is currently unavailable. Please try again later.");
    }
}
