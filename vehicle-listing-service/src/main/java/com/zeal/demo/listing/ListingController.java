package com.zeal.demo.listing;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListingController {

    private static List<Vehicle> vehicleList = new ArrayList<>(10);

    static {
        vehicleList.add(new Vehicle(1, "2008", "Chrysler", "Sebring", "LX"));
        vehicleList.add(new Vehicle(2, "2005", "Ford", "Escape", "XLT"));
        vehicleList.add(new Vehicle(3, "2008", "Hyundai", "Tiburon", "GS"));
        vehicleList.add(new Vehicle(4, "2003", "Dodge", "Stratus", "SE"));
        vehicleList.add(new Vehicle(5, "2005", "Toyota", "Sienna", "LE"));
        vehicleList.add(new Vehicle(6, "2002", "Volkswagon", "Passat", "GLS"));
        vehicleList.add(new Vehicle(7, "2002", "Pontiac", "G6", "1SV"));
        vehicleList.add(new Vehicle(8, "2002", "Honda", "Civic", "EX"));
        vehicleList.add(new Vehicle(9, "2015", "Nissan", "Versa", "Note S"));
        vehicleList.add(new Vehicle(10, "2006", "Cadillac", "CTS", "Sport"));
    }

    @GetMapping("/listings/{id}")
    public Vehicle viewVehicleListing(@PathVariable Integer id) {
        return vehicleList.stream().filter(vehicle -> vehicle.id() == id).findFirst()
                .orElseThrow(() -> new VehicleNotFoundException("Vehicle with id of ["+id+"] not found."));
    }

    @GetMapping("/listings")
    public List<Vehicle> viewAllListings() {

        return vehicleList;
    }

    @GetMapping("/fallback")
    public String fallbackMethod() {
        return "Please try later";
    }

}
