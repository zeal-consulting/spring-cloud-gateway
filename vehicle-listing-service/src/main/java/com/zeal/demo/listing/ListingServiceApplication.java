package com.zeal.demo.listing;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ListingServiceApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(ListingServiceApplication.class).web(WebApplicationType.SERVLET).run(args);
    }
}
