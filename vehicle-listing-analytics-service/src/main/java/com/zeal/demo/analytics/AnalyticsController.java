package com.zeal.demo.analytics;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnalyticsController {

    private List<VehicleRating> ratings = new ArrayList<>();

    private List<Vehicle> likes = new ArrayList<>();

    private static List<Vehicle> vehicleList = new ArrayList<>(10);

    static {
        vehicleList.add(new Vehicle(1, "2008", "Chrysler", "Sebring", "LX"));
        vehicleList.add(new Vehicle(2, "2005", "Ford", "Escape", "XLT"));
        vehicleList.add(new Vehicle(3, "2008", "Hyundai", "Tiburon", "GS"));
        vehicleList.add(new Vehicle(4, "2003", "Dodge", "Stratus", "SE"));
        vehicleList.add(new Vehicle(5, "2005", "Toyota", "Sienna", "LE"));
        vehicleList.add(new Vehicle(6, "2002", "Volkswagon", "Passat", "GLS"));
        vehicleList.add(new Vehicle(7, "2002", "Pontiac", "G6", "1SV"));
        vehicleList.add(new Vehicle(8, "2002", "Honda", "Civic", "EX"));
        vehicleList.add(new Vehicle(9, "2015", "Nissan", "Versa", "Note S"));
        vehicleList.add(new Vehicle(10, "2006", "Cadillac", "CTS", "Sport"));
    }

    @PostMapping("/analytics/vehicles/{vehicleId}/ratings/{rating}")
    public void rateVehicle(@PathVariable Integer vehicleId, @PathVariable Integer rating) {

        Vehicle vehicle = vehicleList.stream().filter(v -> v.id() == vehicleId).findFirst()
                .orElseThrow(() -> new VehicleNotFoundException("Vehicle with id ["+vehicleId+"] not found."));

        ratings.add(new VehicleRating(vehicle, rating));
    }

    @PostMapping("/analytics/vehicles/{vehicleId}/likes")
    public void likeVehicle(@PathVariable Integer vehicleId) {

        Vehicle vehicle = vehicleList.stream().filter(v -> v.id() == vehicleId).findFirst()
                .orElseThrow(() -> new VehicleNotFoundException("Vehicle with id ["+vehicleId+"] not found."));

        likes.add(vehicle);
    }

    @GetMapping("/analytics/ratings")
    public List<VehicleRating> displayRatings() {
        return ratings;
    }

    @GetMapping("/analytics/likes")
    public List<Vehicle> displayLikes() {
        return likes;
    }
}
