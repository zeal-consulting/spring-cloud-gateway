package com.zeal.demo.analytics;

public record VehicleRating(Vehicle vehicle, int rating) {

}
