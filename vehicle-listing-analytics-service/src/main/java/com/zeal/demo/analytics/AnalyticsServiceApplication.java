package com.zeal.demo.analytics;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class AnalyticsServiceApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(AnalyticsServiceApplication.class).web(WebApplicationType.SERVLET).run(args);
    }
}
